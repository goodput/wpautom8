#WPAutoM8#
This is a basic Wordpress Automation Install Script using WP-CLI. This Bash Script Creates a databases, installs Wordpress then creates a repo on Bitbucket Based of what your names your site.
#How To Install#
1) Install WP-CLI 
2) Clone the script into your Root of Development DIR 
3) Add your Wordpress Site Credidentials and Bitbucket Credidentials. 
4) Run Script via Terminal.
#Contributing#
I'm sure there is some sloppy coding on my behalf here as I'm only new to writing Bash Scripts. Please feel to submit any pull requests with what you have changed & reasons why.
I simply made this script as an automation tool, I hope it's helpful to you as well.