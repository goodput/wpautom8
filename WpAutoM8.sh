#############################################################################################
# Basic Wordpress Automation Install
# Written by Taylor goodall || Twitter @theUnleyDads
# Please feel free to contribue as always

###################################################################################################



# - - - - - - - - Configuration - - - - - - - -
GituserName="Bitbucket Email Address"
GitPassWord="BitBucket Password"
gitName="Bitbucket Username"
WpUser="Wordpress Login User"
wpPass="Wordpress Password "
wpEmail="Wordpress Email Address"
# - - - - - - - - - - - - - - - - - - - - - - -
echo "- - - - Run Wordpress Install - - - -"


read -p "Enter WordPress siteName: "  siteName
echo
echo "------- Creating Site: $siteName -------"
echo
echo "------- Thank's we're off to work now -------"
echo
mkdir "$siteName"
cd "$siteName"

wp core download

echo "CREATE DATABASE $siteName" | mysql -u root
wp core config --dbname=$siteName --dbuser=root --dbpass=
wp core install --url=http://$siteName.dev --title=$siteName --admin_user=$WpUser --admin_password=$wpPass --admin_email=$wpEmail

curl --user $GituserName:$GitPassWord https://api.bitbucket.org/1.0/repositories/ \
--data name=$siteName > /dev/null

git init

echo "# $siteName Wordpress Site" > README.md > /dev/null

git add README.md > /dev/null

git commit -m 'first commit' > /dev/null

git remote add origin http://git@bitbucket.org/$gitName/$siteName.git > /dev/null
git push -u origin master > /dev/null
echo
echo "Wordpress Successfully Installed http:/$siteName.dev is live"
